import 'package:flutter/material.dart';

class YesNoModal extends StatelessWidget {
  final String title;
  final String message;
  final String yesText;
  final String noText;
  final Function onYes;
  final Function onNo;

  const YesNoModal({
    Key? key,
    required this.title,
    required this.message,
    required this.yesText,
    required this.noText,
    required this.onYes,
    required this.onNo,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.amber,
      title: Text(title),
      content: Text(message),
      actions: <Widget>[
        TextButton(
          child: Text(yesText),
          onPressed: () {
            Navigator.of(context).pop();
            onYes();
          },
        ),
        TextButton(
          child: Text(noText),
          onPressed: () {
            Navigator.of(context).pop();
            onNo();
          },
        ),
      ],
    );
  }
}
