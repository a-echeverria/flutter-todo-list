import 'package:flutter/material.dart';
import 'package:todolist/todo/todoform.dart';
import 'package:todolist/todo/todolist.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          brightness: Brightness.dark,
          backgroundColor: Colors.green),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> todos = ["Hola", "Bjr", "Hello", "Bye"];

  void addTodo(String todo) {
    if (todo != "") {
      todos.add(todo);
      setState(() {});
    }
  }

  void removeTodo(int index) {
    todos.removeAt(index);
    setState(() {});
  }

  void editTodo(int index, String todo) {
    todos[index] = todo;
    setState(() {});
  }

  void callTodoForm() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => TodoForm(
                onSave: (String todo) {
                  addTodo(todo);
                }
              )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Center(
        child: Container(
          color: Colors.pink,
          child: TodoList(todos: todos, onDelete: removeTodo, onEdit: editTodo),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
        ),
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: callTodoForm,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
