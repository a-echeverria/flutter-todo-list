import 'package:flutter/material.dart';
import 'package:todolist/common/modal.dart';
import 'package:todolist/todo/todoform.dart';

class TodoList extends StatelessWidget {
  final Function(int) onDelete;
  final Function(int, String) onEdit;
  final List<String> todos;

  const TodoList(
      {Key? key,
      required this.todos,
      required this.onDelete,
      required this.onEdit})
      : super(key: key);

  void displayModal(BuildContext context, int index) {
    showDialog(
        context: context,
        builder: (context) {
          return YesNoModal(
            title: 'Attention',
            message: 'Voulez-vous vraiment supprimer cette tâche ?',
            yesText: 'Oui',
            noText: 'Non',
            onYes: () {
              onDelete(index);
            },
            onNo: () {},
          );
        });
  }

  void navigateToEditForm(BuildContext context, int index) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => TodoForm(
              todo: this.todos[index],
              onSave: (String todo) {
                onEdit(index, todo);
              },
          ),
      )

    );
  }

  Widget _buildBody(BuildContext context) {
    if ( !(todos.length > 0) ) {
      return CircularProgressIndicator();
    }

    return  Column(
      children: [
        Container(
            height: MediaQuery.of(context).size.height * 0.8,
            child: ListView.builder(
                itemCount: todos.length,
                itemBuilder: (BuildContext context, int index) {
                  String todo = todos[index];
                  return ListTile(
                    tileColor: Colors.white,
                    trailing: Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      child: Row(
                         children: [
                           IconButton(
                               icon: Icon(Icons.delete),
                               color: Colors.red,
                               onPressed: () {
                                 this.displayModal(context,index);
                               },
                             ),
                           IconButton(
                             color: Colors.black,
                             icon: const Icon(Icons.edit),
                             onPressed: () {
                               navigateToEditForm(context, index);
                             },
                           ),

                         ],
                      ),
                    ),
                    title: Center(
                      child: Text(
                        todo, style:
                      TextStyle(color:  Colors.black),
                      ),
                    ),

                  );

                }
            ),
            // child: ListView.builder(
            //   itemCount: todos.length,
            //   itemBuilder: (context, index) {
            //     return ListTile(
            //       trailing: Row(
            //         children: [
            //           IconButton(
            //             icon: const Icon(Icons.delete),
            //             onPressed: () {
            //               displayModal(context, index);
            //             },
            //           ),
            //           IconButton(
            //             icon: const Icon(Icons.edit),
            //             onPressed: () {
            //               Navigator.push(
            //                 context,
            //                 MaterialPageRoute(
            //                     builder: (context) => TodoForm(
            //                           callBack: (String todo, int? index) {
            //                             onEdit(index!, todo);
            //                           },
            //                         )),
            //               );
            //             },
            //           ),
            //         ],
            //       ),
            //       tileColor: Colors.white,
            //       // title: Text(
            //       //     todos[index],
            //       //     style: const TextStyle(color: Colors.black)),
            //     );
            //   },
            // )
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {



    return Scaffold(
        appBar: AppBar(
          title: const Text('Ma todo list :)'),
        ),
      body: _buildBody(context)
    );
    return Scaffold(
      appBar: AppBar(
        title: const Text('Ma todo list :)'),
      ),
      body: Column(children: [
        Container(
            height: MediaQuery.of(context).size.height * 0.8,
            child: ListView.builder(
              itemCount: todos.length,
              itemBuilder: (context, index) {
                return ListTile(
                  trailing: Row(
                    children: [
                      // IconButton(
                      //   icon: const Icon(Icons.delete),
                      //   onPressed: () {
                      //     displayModal(context, index);
                      //   },
                      // ),
                      // IconButton(
                      //   icon: const Icon(Icons.edit),
                      //   onPressed: () {
                      //     Navigator.push(
                      //       context,
                      //       MaterialPageRoute(
                      //           builder: (context) => TodoForm(
                      //                 callBack: (String todo, int? index) {
                      //                   onEdit(index!, todo);
                      //                 },
                      //               )),
                      //     );
                      //   },
                      // ),
                    ],
                  ),
                  tileColor: Colors.white,
                  // title: Text(
                  //     todos[index],
                  //     style: const TextStyle(color: Colors.black)),
                );
              },
            )
        )
      ]),
    );
  }
}
