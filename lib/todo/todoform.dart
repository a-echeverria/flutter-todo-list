import 'package:flutter/material.dart';

class TodoForm extends StatelessWidget {

  void Function(String)? onSave;

  String? todo;

  TodoForm(
      {Key? key, this.onSave, this.todo})
      : super(key: key);

  final TextEditingController _todoController = TextEditingController();

  void saveTodo(BuildContext context) {
    final String todo = _todoController.text;
    if (this.onSave != null) {
      this.onSave!(todo);
    }

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    _todoController.text = this.todo ?? '';
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            TextField(
              controller: _todoController,
              decoration: const InputDecoration(
                labelText: 'Todo',
              ),
            ),
            TextButton(
              child: const Text('Save'),
              onPressed: () {
                saveTodo(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
